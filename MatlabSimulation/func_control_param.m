function [Kp, Kd] = func_control_param

Kp = 1e3*eye(5);
Kd = 1e2*eye(5);

end