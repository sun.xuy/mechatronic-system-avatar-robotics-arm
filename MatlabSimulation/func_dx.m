
function dx = func_dx(t,x, q_d, dq_d)

q = reshape(x(1:5),5,1);
dq = reshape(x(6:10),5,1);

model_params = func_model_params;

[D,H,B] = DHB_matrices(q,dq,model_params);

fx = [dq; D\(-H)];
gx = [zeros(5,5); D\B];

u = func_feedback(q,dq,model_params, q_d, dq_d);

% u = zeros(5,1);

dx = fx + gx*u;

end