
% set up the params of the system
% 12/03/2019 Writen by Xuyang Sun, Boston, MA

function model_params = func_model_params

m1 = 2;
m2 = 1;
m3 = 1;
J2 = 0.5;
J3 = 0.2;
g = 9.81;
l1 = 0.15;
l2 = 0.5;
l3 = 0.2;
l4 = 0.4;
theta1 = 30*pi/180;
xOffset = 0;
yOffset = 1;
zOffset = 1;

model_params = [m1 m2 m3 J2 J3 l1 l2 l3 l4 g theta1 xOffset yOffset zOffset];

end