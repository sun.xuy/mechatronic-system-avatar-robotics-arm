

% symbolically generate all the equations
% 12/03/2019 Writen by Xuyang Sun, Boston, MA



syms q1 q2 q3 q4 q5 dq1 dq2 dq3 dq4 dq5 real
syms theta1 xOffset yOffset zOffset l1 l2 l3 l4 m1 m2 m3 J2 J3 g 

q = [q1 q2 q3 q4 q5].';
dq = [dq1 dq2 dq3 dq4 dq5].';

model_params = [m1 m2 m3 J2 J3 l1 l2 l3 l4 g theta1 xOffset yOffset zOffset];

q2_pr = (q2+q3)/2;
q3_pr = (q2-q3)/2;
q4_pr = (q4+q5)/2;
q5_pr = (q4-q5)/2;

% rotation around z
Z = @(z,q)    [1       0          0       0;...
               0   cos(q)    -sin(q)      0;...
               0   sin(q)     cos(q)      z;...
               0       0          0       1] ;

% rotation around y
Y = @(y,q)       [   cos(q)     0     sin(q)  0;...
                         0      1         0   y;...
                    -sin(q)     0     cos(q)  0;...
                         0      0         0   1];
                     
% rotation around x

X = @(y,q)      [1  0           0  0;...
                 0  cos(q) -sin(q) y;...
                 0  sin(q)  cos(q) 0;...
                 0      0       0  1];
                     
                     
% translation matrix

T = @(x,y,z) [  1   0   0   x;...
                0   1   0   y;...
                0   0   1   z;...
                0   0   0   1];

            
            
% from world frame to base of the robotic arm
Tb0_pr = T(xOffset,yOffset,zOffset);
% rotation matrix of the adjacent link
T0_pr0 = X(0,-theta1);
T01 = Z(0,q1);
T12 = X(l1,q2_pr);
T23 = Y(l2,q3_pr);
T34 = X(l3,q4_pr);
T45 = Y(l4,q5_pr);



% homogenous transformation
P5 = Tb0_pr*T0_pr0*T01*T12*T23*T34*T45;
P4 = Tb0_pr*T0_pr0*T01*T12*T23*T34;
P3 = Tb0_pr*T0_pr0*T01*T12*T23;
P2 = Tb0_pr*T0_pr0*T01*T12;
P1 = Tb0_pr*T0_pr0*T01;
P0 = Tb0_pr*T0_pr0;

% obtain the position of each joint
p5 = P5(1:3,4);
p4 = P4(1:3,4);
p3 = P3(1:3,4);
p2 = P2(1:3,4);
p1 = P1(1:3,4);




V2 = jacobian(p2,q)*dq;
V4 = jacobian(p4,q)*dq;
V5 = jacobian(p5,q)*dq;

dq3_pr = jacobian(q3_pr,q)*dq;
dq5_pr = jacobian(q5_pr,q)*dq;

K_trans = 1/2*m1*(V2.')*V2 + 1/2*m2*(V4.')*V4 + 1/2*m3*(V5.')*V5;
K_rot = 1/2*J2*dq3_pr.'*dq3_pr + 1/2*J3*dq5_pr.'*dq5_pr;

K = K_rot + K_trans;

V = m1*g*p2(3) + m2*g*p4(3) + m3*g*p5(3);

D = jacobian(jacobian(K,dq).',dq); D = simplify(D);

G = jacobian(V,q).'; G = simplify(G);

temp_C = jacobian(D*dq,q) - (1/2)*jacobian(D*dq,q).';

Cdq = simplify(temp_C*dq);

H = simplify(Cdq + G);

B = sym(eye(5));

matlabFunction(D,H,B,'File','DHB_matrices.m','Vars',{q, dq, model_params},'Outputs',{'D','H','B'})













