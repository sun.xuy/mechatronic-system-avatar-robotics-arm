
% This is the forward kinematics of the robotic arm
% 12/03/2019 Writen by Xuyang Sun, Boston, MA

function [p1,p2,p3,p4,p5] = func_fwdK(q1,q2,q3,q4,q5)

% params;

model_params = func_model_params;

% [m1 m2 m3 J2 J3 l1 l2 l3 l4 g theta1 xOffset yOffset zOffset]

l1 = model_params(6);
l2 = model_params(7);
l3 = model_params(8);
l4 = model_params(9);

theta1 = model_params(11);

xOffset = model_params(12);
yOffset = model_params(13);
zOffset = model_params(14);

% mapping of the differential
q2_pr = (q2+q3)/2;
q3_pr = (q2-q3)/2;
q4_pr = (q4+q5)/2;
q5_pr = (q4-q5)/2;

% rotation around z
Z = @(z,q)    [1       0          0       0;...
               0   cos(q)    -sin(q)      0;...
               0   sin(q)     cos(q)      z;...
               0       0          0       1] ;

% rotation around y
Y = @(y,q)       [   cos(q)     0     sin(q)  0;...
                         0      1         0   y;...
                    -sin(q)     0     cos(q)  0;...
                         0      0         0   1];
                     
% rotation around x

X = @(y,q)      [1  0           0  0;...
                 0  cos(q) -sin(q) y;...
                 0  sin(q)  cos(q) 0;...
                 0      0       0  1];
                     
                     
% translation matrix

T = @(x,y,z) [  1   0   0   x;...
                0   1   0   y;...
                0   0   1   z;...
                0   0   0   1];

            
            
% from world frame to base of the robotic arm
Tb0_pr = T(xOffset,yOffset,zOffset);
% rotation matrix of the adjacent link
T0_pr0 = X(0,-theta1);
T01 = Z(0,q1);
T12 = X(l1,q2_pr);
T23 = Y(l2,q3_pr);
T34 = X(l3,q4_pr);
T45 = Y(l4,q5_pr);



% homogenous transformation
P5 = Tb0_pr*T0_pr0*T01*T12*T23*T34*T45;
P4 = Tb0_pr*T0_pr0*T01*T12*T23*T34;
P3 = Tb0_pr*T0_pr0*T01*T12*T23;
P2 = Tb0_pr*T0_pr0*T01*T12;
P1 = Tb0_pr*T0_pr0*T01;
P0 = Tb0_pr*T0_pr0;

% obtain the position of each joint
p5 = P5(1:3,4);
p4 = P4(1:3,4);
p3 = P3(1:3,4);
p2 = P2(1:3,4);
p1 = P1(1:3,4);



end














