function u = func_feedback(q,dq,model_params, q_d, dq_d)

[D,H,B] = DHB_matrices(q,dq,model_params);

eps = normrnd(0.1*pi/180, 0.02*pi/180)*ones(5,1);
y = q - q_d + eps;
dy = dq - dq_d;

[Kp, Kd] = func_control_param;

v = Kp*y + Kd*dy;

u = B\(H - v);

end