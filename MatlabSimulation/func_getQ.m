
function F = func_getQ(q0,traj)


    q1 = q0(1);
    q2 = q0(2);
    q3 = q0(3);
    q4 = q0(4);
    q5 = q0(5);
 
 [~,~,~,~,p5] = func_fwdK(q1,q2,q3,q4,q5);
    
    F(1) = p5(1)-traj(1);
    F(2) = p5(2)-traj(2);
    F(3) = p5(3)-traj(3);
    F(4) = 0;
    F(5) = 0;
    

 
end






